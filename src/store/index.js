import { config } from './modules/config';
import { trips } from './modules/trips';
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
const initialState = () => ({});
const store = new Vuex.Store({
  state: initialState,
  mutations: {
    resetState: (state) => {
      Object.assign(state, initialState());
    },
  },
  actions: {
    resetState: ({ commit, dispatch }) => {
      commit('resetState');
      dispatch('config/resetState', null, { root: true });
      dispatch('trips/resetState', null, { root: true });
    },
  },
  modules: {
    config,
    trips,
  },
});
export default store;

import { calcTrip } from '@/features/shuttlingTask/shuttlingService';
const initialState = () => ({
  sum: 0, // for calc in logic only value not for user
  running: false,
  isTimingRunning: false,
  trips: [
    {
      totalTime: 0, //ft + duration + conf time(rt)
      statistics: null,
      trip: [
        {
          id: 'ss',
          totalTime: 0,
          // nextFt: 0, // ft + leg + st
          station: {
            location: null,
            inTime: null,
            outTime: null,
          },
        },
        {
          id: 'es',
          totalTime: 0,
          station: {
            location: null,
            inTime: null,
            outTime: null,
          },
        },
      ],
    },
  ],
  baseTrip: null,
});
export const trips = {
  namespaced: true,
  state: initialState,
  // Todo:use constant for mutations
  mutations: {
    setBaseTrip(state, changes) {
      state.baseTrip = {
        ...changes,
        statistics: { ...changes.statistics },
        trip: [...changes.trip],
      };
    },
    setIsTimingRunning(state, changes) {
      state.isTimingRunning = changes;
    },
    updateStationTotalTime(state, { tripIndex, stationIndex, totalTime }) {
      state.trips[tripIndex].trip[stationIndex].totalTime = totalTime;
    },
    addNewTrip(state) {
      // const cloneFromIndex = changes?.index || state.trips.length - 1;
      let newRun;
      if (state.trips.length % 2 != 0)
        // if next index odd do reverse else don't
        newRun = {
          totalTime: 0, // check value
          statistics: null,
          trip: [
            ...[
              ...state.baseTrip.trip.map((s) => ({
                id: s.id,
                totalTime: 0,
                station: {
                  location: s.station.location,
                  inTime: null,
                  outTime: null,
                },
              })),
            ].reverse(),
          ],
        };
      else
        newRun = {
          totalTime: 0, // check value
          statistics: null,
          trip: [
            ...[
              ...state.baseTrip.trip.map((s) => ({
                id: s.id,
                totalTime: 0,
                station: {
                  location: s.station.location,
                  inTime: null,
                  outTime: null,
                },
              })),
            ],
          ],
        };
      state.trips.push(newRun);
    },
    updateTripStatistics(state, { index, statistics }) {
      if (state.trips[index])
        state.trips[index].statistics = {
          ...(state.trips[index].statistics || {}),
          ...statistics,
        };
      else console.error(`state.trips[index] with index ${index} not found`);
    },
    updateTripTotalTime(state, { index, totalTime }) {
      state.trips[index].totalTime = totalTime;
    },
    updateTripPath(state, { index, path }) {
      state.trips[index].trip = [...path];
    },
    addStop(state, { index, nextStopId }) {
      const newStop = {
        id: nextStopId,
        totalTime: 0,
        station: {
          location: null,
          inTime: null,
          outTime: null,
        },
      };
      const insertedStopIndex = state.trips[index].trip.length - 1;
      state.trips[index].trip.splice(insertedStopIndex, 0, newStop);
      // may need make trip dirty as flag key
    },
    removeStop(state, { index, stop }) {
      const stopIndex = state.trips[index].trip.indexOf(stop);
      if (stopIndex !== -1) state.trips[index].trip.splice(stopIndex, 1);
      // may need make trip dirty as flag key
    },
    resetState: (state) => {
      Object.assign(state, initialState());
    },
  },
  actions: {
    removeExceededStations({ dispatch, state, rootState, rootGetters }) {
      let needExtraCheck = false;
      const lastTrip = state.trips.at(-1).trip;
      if (lastTrip.at(-1).totalTime == 0) return; // still not yet calc need enhancement=> return avoid wasted search.
      const extraStationsStartingIndex = lastTrip.findIndex((tripPoint) => {
        return (
          tripPoint.totalTime >
          rootGetters['config/shuttleDuration'] +
            rootState.config.tripEndTimeTolerance
        );
      });
      if (extraStationsStartingIndex == 0) needExtraCheck = true; // if first station of last trip Exceeded so the prev trip could also Exceeded in time

      if (extraStationsStartingIndex != -1) {
        // remove extra station in last trip after deadline
        state.trips.at(-1).trip.splice(extraStationsStartingIndex);

        if (state.trips.at(-1).trip.length > 0)
          // if there is still stations in last trip
          state.sum = state.trips.at(-1).trip.at(-1).totalTime;
        // reset sum to the new total of new last station;
        else {
          // state.trips.at(-1) // empty so take the -2
          state.sum = state.trips.at(-2).totalTime; // reset sum ;
        }
        // if last trip has only one station ( starting station) remove that trip
        if (state.trips.length == 1) {
          // if we have only the first base run don't remove
          console.error('base trip exceeded the allowed shuttling time');
          if (needExtraCheck) dispatch('removeExceededStations');
          return;
        }

        if (
          state.trips.at(-1).trip.length == 1 ||
          state.trips.at(-1).trip.length == 0
        ) {
          state.trips.splice(-1);
        }
        if (needExtraCheck) dispatch('removeExceededStations');
      }
    },
    async calcTrip(state, trip) {
      return await calcTrip(trip);
    },
    resetBeforeGenerate({ state, commit, getters, rootGetters }) {
      state.sum = rootGetters['config/fromTimeInSeconds'];
      state.trips = [state.trips[0]];
      if (!getters.getBaseTrip) commit('setBaseTrip', state.trips[0]);
    },
    async generateRuns(
      { state, rootState, rootGetters, commit, dispatch },
      init
    ) {
      if (init == undefined) {
        dispatch('resetBeforeGenerate');
        state.running = true;
      }
      let index = init || 0;
      const { duration, legs } = await dispatch(
        'calcTrip',
        state.trips[index].trip
      );

      if (index > 0) {
        state.sum += rootState.config.durationBetweenTripRuns;
      }
      state.sum += duration;
      state.sum += rootState.config.stopDuration * (legs.length - 1);
      if (
        rootGetters['config/shuttleDuration'] +
          rootState.config.tripEndTimeTolerance >
        state.sum
      ) {
        commit('addNewTrip', { index });
        index += 1;
        return dispatch('generateRuns', index);
      } else {
        // maybe here should subtract last sum addition but i reset sum after remove now instead.
        state.running = false;
      }
    },
    resetState: ({ commit }) => {
      commit('resetState');
    },
  },
  getters: {
    getBaseTrip(state) {
      return state.baseTrip;
    },
    isStillGenerate(state) {
      return state.running;
    },
    IsTimingRunning(state) {
      return state.isTimingRunning;
    },
    isProcessDone(state, getters) {
      return {
        done: !getters.IsTimingRunning && !getters.isStillGenerate,
        IsTimingRunning: getters.IsTimingRunning,
        isStillGenerate: getters.isStillGenerate,
      };
    },
    getTrips(state) {
      return state.trips;
    },
    getFirstTrip(state) {
      return state.trips[0];
    },
    getTripByIndex: (state) => (index) => {
      return state.trips[index];
    },
    getActualTotalDuration(state) {
      return state?.trips?.at(-1)?.totalTime || 0;
    },
    getSum(state) {
      return state.sum;
    },
    getTotalDistance(state) {
      return state.trips?.reduce((acc, curTrip) => {
        acc += curTrip?.statistics?.distance || 0;
        return acc;
      }, 0);
    },
  },
};

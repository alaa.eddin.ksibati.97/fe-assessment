import moment from 'moment';
const initialState = () => ({
  tripEndTimeTolerance: 0,
  durationBetweenTripRuns: 0,
  stopDuration: 1500,
  fromTime: moment().format('HH:mm'),
  toTime: moment().format('HH:mm'),
});
export const config = {
  namespaced: true,
  state: initialState,
  mutations: {
    updateFromTime(state, changes) {
      state.fromTime = changes;
    },
    updateToTime(state, changes) {
      state.toTime = changes;
    },
    updateTripEndTimeTolerance(state, changes) {
      state.tripEndTimeTolerance = changes;
    },
    updateDurationBetweenTripRuns(state, changes) {
      state.durationBetweenTripRuns = changes;
    },
    updateStopDuration(state, changes) {
      state.stopDuration = changes;
    },
    resetState: (state) => {
      Object.assign(state, initialState());
    },
  },
  actions: {
    resetState: ({ commit }) => {
      commit('resetState');
    },
  },
  getters: {
    getFromTime(state) {
      return state.fromTime;
    },
    getToTime(state) {
      return state.toTime;
    },
    shuttleDuration(state, getters) {
      const startTime = moment(state.fromTime, 'HH:mm');
      const endTime = moment(state.toTime, 'HH:mm');
      if (endTime.diff(startTime, 'seconds') > 0)
        return (
          Math.abs(endTime.diff(startTime, 'seconds')) +
          getters.fromTimeInSeconds
        );
      else return getters.fromTimeInSeconds;
      // const diffInSeconds = endTime.diff(startTime, 'seconds');
      // return Math.abs(endTime.diff(startTime, 'seconds'));
    },
    fromTimeInSeconds(state) {
      const fromTime = moment
        .duration(moment(state.fromTime, 'HH:mm').diff(moment().startOf('day')))
        .asSeconds();
      return fromTime;
    },
    getTripEndTimeTolerance(state) {
      return state.tripEndTimeTolerance;
    },
    getDurationBetweenTripRuns(state) {
      return state.durationBetweenTripRuns;
    },
    getStopDuration(state) {
      return state.stopDuration;
    },
    getRegenerateOrRemoveDependencies(state, getters, rootState, rootGetters) {
      return (
        getters.shuttleDuration +
        getters.getTripEndTimeTolerance -
        getters.getDurationBetweenTripRuns -
        getters.getStopDuration -
        rootGetters['trips/getActualTotalDuration']
      );
    },
  },
};

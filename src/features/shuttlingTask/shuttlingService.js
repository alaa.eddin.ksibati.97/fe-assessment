import api from '@/lib/axios';
import moment from 'moment';
export const calcTrip = async (trip) => {
  const payload = {
    way_points: [
      ...trip.map(({ station }) => [
        station.location.lat,
        station.location.lng,
      ]),
    ],
    geometry: 'geojson',
    overview: 'full',
  };
  try {
    let { data } = await api.post('/common-master/osrm/polyline', payload);
    return data.result;
  } catch (error) {
    console.error(error);
  }
};

export function timeFromSecondes(durationInSeconds) {
  if (durationInSeconds == '--') return durationInSeconds;
  return moment
    .utc(moment.duration(durationInSeconds, 'seconds').asMilliseconds())
    .format('h:mm A');
}
export function durationFromSecondes(durationInSeconds) {
  if (durationInSeconds == '--') return durationInSeconds;
  const duration = moment.duration(durationInSeconds, 'seconds');
  const hours = duration.hours().toString().padStart(2, '0');
  const minutes = duration.minutes().toString().padStart(2, '0');
  const seconds = duration.seconds().toString().padStart(2, '0');
  return `${hours}:${minutes}:${seconds}`;
}

export function distanceFormatter(distanceInMeters = 0) {
  const distanceInKilometers = Math.floor(distanceInMeters / 1000);
  const distanceInMetersRemainder = Math.floor(distanceInMeters % 1000);
  return `${distanceInKilometers} km, ${distanceInMetersRemainder} m`;
}

export function isSameArray(arr1, arr2) {
  if (!(Array.isArray(arr1) && Array.isArray(arr2))) return false;
  if (arr1.length !== arr2.length) {
    return false;
  }
  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) {
      return false;
    }
  }
  return true;
}

export function geometryLatLngFromLngLat(geometry = []) {
  return geometry.map(([lng, lat]) => [lat, lng]);
}

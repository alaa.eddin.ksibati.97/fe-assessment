export const mockLocationOptions = {
  result: {
    addresses: [
      {
        address:
          '1 Sheikh Mohammed bin Rashid Blvd - Downtown Dubai - Dubai - United Arab Emirates',
        lat: 25.197197,
        lng: 55.27437639999999,
      },
      {
        address:
          '1 Sheikh Mohammed bin Rashid Blvd - Downtown Dubai - Dubai - United Arab Emirates',
        lat: 25.1975148,
        lng: 55.27487319999999,
      },
      {
        address:
          '161 Sheikh Rashid Rd - Oud Metha - Dubai - United Arab Emirates',
        lat: 25.2330247,
        lng: 55.31008019999999,
      },
      {
        address: '2465 S Industrial Hwy, Ann Arbor, MI 48104, USA',
        lat: 42.2494444,
        lng: -83.73111109999999,
      },
      {
        address:
          '3035 King Faisal W, King Fahd Industrial Port, 6812، Al Jubayl 35514, Saudi Arabia',
        lat: 26.9970994,
        lng: 49.6430612,
      },
      {
        address: '43 27th St - Port Saeed - Dubai - United Arab Emirates',
        lat: 25.2536218,
        lng: 55.3354911,
      },
      {
        address:
          '6th St - Al Qusais - Al Qusais 1 - Dubai - United Arab Emirates',
        lat: 25.2823974,
        lng: 55.3691058,
      },
      {
        address:
          '79M9+CH2 - 6th St - Al Qusais - Al Qusais 1 - Dubai - United Arab Emirates',
        lat: 25.2835191,
        lng: 55.3688786,
      },
      {
        address:
          'Abu Dhabi -Dubai Rd - Exit 399 - Sheikh Makhtoum Bin Rashid Street - Ghantout - Abu Dhabi - United Arab Emirates',
        lat: 24.8645592,
        lng: 54.9056213,
      },
      {
        address:
          'Al Bateen Marina King Abdullah Bin Abdulaziz Al Saud Street - شارع القُفَّال - البطين - أبو ظبي - United Arab Emirates',
        lat: 24.448734,
        lng: 54.3375721,
      },
      {
        address: 'Al Jazira St - Deira - Dubai - United Arab Emirates',
        lat: 25.2680251,
        lng: 55.3207507,
      },
      {
        address: 'Al Raffa - Dubai - United Arab Emirates',
        lat: 25.255758,
        lng: 55.28808739999999,
      },
      {
        address: 'Al Safa - Al Safa 2 - Dubai - United Arab Emirates',
        lat: 25.1586178,
        lng: 55.2245398,
      },
      {
        address: 'Al Waha - Dubai - United Arab Emirates',
        lat: 25.0295157,
        lng: 55.2860145,
      },
      {
        address:
          'Al Wazn St - Abu Dhabi Industrial City - ICAD I - Abu Dhabi - United Arab Emirates',
        lat: 24.3331427,
        lng: 54.5239257,
      },
      {
        address: 'Beach Rd, Neelankarai, Chennai, Tamil Nadu 600041, India',
        lat: 12.9510819,
        lng: 80.261547,
      },
      {
        address:
          'Blue 3 Tower - Al Nahda - Al Nahda 2 - Dubai - United Arab Emirates',
        lat: 25.296605,
        lng: 55.38517659999999,
      },
      {
        address: 'Colonia Miramontes, Tegucigalpa, Honduras',
        lat: 14.0833441,
        lng: -87.1952775,
      },
      {
        address:
          'Consulates Area, Sheikh Khalifa Bin Zayed Road, Bur Dubai - Umm Hurair 1 - Dubai - United Arab Emirates',
        lat: 25.257404,
        lng: 55.30661199999999,
      },
      {
        address: 'Downtown Dubai - Dubai - United Arab Emirates',
        lat: 25.1972295,
        lng: 55.279747,
      },
      {
        address: 'Downtown Dubai - Dubai - United Arab Emirates',
        lat: 25.198765,
        lng: 55.2796053,
      },
      {
        address: 'Duba Saudi Arabia',
        lat: 27.3457474,
        lng: 35.7243073,
      },
      {
        address: 'Dubai - United Arab Emirates',
        lat: 25.2048493,
        lng: 55.2707828,
      },
      {
        address: 'Dubna, Moscow Oblast, Russia',
        lat: 56.7320202,
        lng: 37.1668974,
      },
      {
        address: 'Dubois Area School District, PA, USA',
        lat: 41.0850358,
        lng: -78.7022678,
      },
      {
        address: 'Dubysos g. 31, 91181 Klaipėda, Lithuania',
        lat: 55.692192,
        lng: 21.1668796,
      },
      {
        address:
          'IBN Battuta Bus Station jebel ali - Jebel Ali Village - Dubai - United Arab Emirates',
        lat: 25.046352,
        lng: 55.11780899999999,
      },
      {
        address:
          'Ibn Battuta St - Jebel Ali Village - Al Furjan - Dubai - United Arab Emirates',
        lat: 25.0335571,
        lng: 55.1477492,
      },
      {
        address:
          'Ibn Battuta St - Jebel Ali Village - Dubai - United Arab Emirates',
        lat: 25.038961,
        lng: 55.14033,
      },
      {
        address:
          'Jebel Ali Village - Discovery Gardens - Dubai - United Arab Emirates',
        lat: 25.0390117,
        lng: 55.1444815,
      },
      {
        address: 'Jumeirah - Dubai - United Arab Emirates',
        lat: 25.2016428,
        lng: 55.2452567,
      },
      {
        address: 'Marina Mall, Egattur, Tamil Nadu 600130',
        lat: 12.8360623,
        lng: 80.22951719999999,
      },
      {
        address:
          'Me 7, Al Mussafah - Musaffah - Musaffah Industrial - Abu Dhabi - United Arab Emirates',
        lat: 24.378656,
        lng: 54.5175065,
      },
      {
        address: 'Mushrif - Dubai - United Arab Emirates',
        lat: 25.2077215,
        lng: 55.45269949999999,
      },
      {
        address: 'Quartier Industriel, Agadir 80000, Morocco',
        lat: 30.4183977,
        lng: -9.579866400000002,
      },
      {
        address: 'Route Du Port, Dakhla 73000',
        lat: 23.6845114,
        lng: -15.9492474,
      },
      {
        address:
          'Sheikh Zayed Rd - Dubai Marina - Dubai - United Arab Emirates',
        lat: 25.07643,
        lng: 55.140504,
      },
      {
        address:
          'Sheikh Zayed Rd - Jebel Ali Village - Dubai - United Arab Emirates',
        lat: 25.0445481,
        lng: 55.12029649999999,
      },
      {
        address:
          'Sheikh Zayed Rd - opp. Mashreq Metro Station - Al Barsha - Al Barsha 1 - Dubai - United Arab Emirates',
        lat: 25.1123734,
        lng: 55.1885458,
      },
      {
        address:
          'Sky View 10, Urban Terrace, Upper Ground North Lobby in Sy. No.83/1 of Raidurg Panmakatha Village Serilingampally, Mandal, Silpa Gram Craft Village, Madhapur, Hyderabad, Telangana 500081, India',
        lat: 17.4305924,
        lng: 78.38135849999999,
      },
      {
        address:
          'Street no. 17 Mussaffah Industrial Area - Musaffah - ICAD I - Abu Dhabi - United Arab Emirates',
        lat: 24.3401332,
        lng: 54.5188159,
      },
      {
        address:
          'Tower B, Mohammed bin Zayed Stadium, Al Jazira Club, Opposite to Dusit Thani Sultan Bin Zayed the First St - Muroor Rd - Al Nahyan - E22-02 - Abu Dhabi - United Arab Emirates',
        lat: 24.4524658,
        lng: 54.3934446,
      },
      {
        address: 'Unit 11/39-43 Duerdin St, Notting Hill VIC 3168, Australia',
        lat: -37.9100161,
        lng: 145.1471546,
      },
      {
        address: 'West Bay Beach, West Bay, Honduras',
        lat: 16.2727698,
        lng: -86.6003885,
      },
      {
        address:
          'Zona e Re Industriale, Veternik 10000 Prishtine Prishtina XK, 10000',
        lat: 42.6329186,
        lng: 21.1519423,
      },
      {
        address: 'Zone Industriel Tassila, Dcheira El Jihadia 80000, Morocco',
        lat: 30.3895596,
        lng: -9.5256331,
      },
      {
        address: 'Zone Industrielle Ain Sebaa - Bernoussi, Casablanca, Morocco',
        lat: 33.6270277,
        lng: -7.5111876,
      },
      {
        address:
          'near Rak Bank - Industrial Area - M-11 - Abu Dhabi - United Arab Emirates',
        lat: 24.3638832,
        lng: 54.509986,
      },
      {
        address: 'شارع جميرا، - Umm Suqeim 3 - Dubai - United Arab Emirates',
        lat: 25.1411914,
        lng: 55.18524679999999,
      },
    ],
  },
};

export const mockCalc = {
  result: {
    geometry: [
      [55.274314, 25.197631],
      [55.274843, 25.197843],
      [55.31, 25.232901],
      [55.108259, 25.037302],
      [55.075115, 24.95379],
      [54.944206, 24.901596],
      [54.71808, 24.623051],
      [54.632486, 24.334354],
      [54.346588, 24.15844],
      [53.701836, 24.004546],
      [52.909343, 24.102285],
      [52.760221, 24.050208],
      [52.368959, 24.016913],
      [52.217916, 23.94892],
      [51.898143, 23.935261],
      [51.693565, 24.101697],
      [51.584349, 24.120402],
      [51.488868, 24.191231],
      [51.319475, 24.192051],
      [51.237339, 24.361129],
      [50.96928, 24.489254],
      [50.791582, 24.627595],
      [50.73169, 24.718488],
      [50.73768, 24.818624],
      [50.526055, 24.850032],
      [50.201378, 25.032004],
      [50.04848, 25.04289],
      [49.929714, 25.220481],
      [49.783505, 25.295778],
      [49.775952, 25.498533],
      [49.636941, 25.568002],
      [49.549456, 25.565565],
      [49.5074, 25.699284],
      [49.613975, 25.925107],
      [50.046952, 26.305624],
      [49.919945, 26.676645],
      [49.643033, 26.997303],
    ],
    duration: 37722.4,
    distance: 970787.8,
    legs: [
      {
        weight: 11.3,
        distance: 60.9,
        summary: '',
        duration: 11.3,
      },
      {
        weight: 637.8,
        distance: 8611.6,
        summary: '',
        duration: 637.8,
      },
      {
        weight: 37073.3,
        distance: 962115.3,
        summary: '',
        duration: 37073.3,
      },
    ],
  },
};

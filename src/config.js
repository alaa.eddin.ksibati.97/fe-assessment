export default {
  "development": {
    "config_id": "development",
    "api_url": process.env.VUE_APP_API_URL,
    "socket_url": process.env.VUE_APP_SOCKET_URL,
    "app_name": "my app",
    "app_desc": "my app desc",
    "api_port": process.env.VUE_APP_API_PORT,
    "socket_port": process.env.VUE_APP_SOCKET_PORT,
    "asset_url": process.env.VUE_APP_ASSET_URL,
    "open_map_key": process.env.VUE_APP_OPEN_MAP_KEY,
    "open_map_street_url": process.env.VUE_APP_OSM_URL_STREET,
    "open_map_satellite_url": process.env.VUE_APP_OSM_URL_SATELLITE,
    "json_indentation": 4,
    "database": ""
  },
  "testing": {
    "config_id": "testing",
    "api_url": process.env.VUE_APP_API_URL,
    "socket_url": process.env.VUE_APP_SOCKET_URL,
    "app_name": "my app",
    "app_desc": "my app desc",
    "api_port": process.env.VUE_APP_API_PORT,
    "socket_port": process.env.VUE_APP_SOCKET_PORT,
    "asset_url": process.env.VUE_APP_ASSET_URL,
    "open_map_key": process.env.VUE_APP_OPEN_MAP_KEY,
    "open_map_street_url": process.env.VUE_APP_OSM_URL_STREET,
    "open_map_satellite_url": process.env.VUE_APP_OSM_URL_SATELLITE,
    "json_indentation": 4,
    "database": ""
  },
  "staging": {
    "config_id": "staging",
    "api_url": process.env.VUE_APP_API_URL,
    "socket_url": process.env.VUE_APP_SOCKET_URL,
    "app_name": "my app",
    "app_desc": "my app desc",
    "api_port": process.env.VUE_APP_API_PORT,
    "socket_port": process.env.VUE_APP_SOCKET_PORT,
    "asset_url": process.env.VUE_APP_ASSET_URL,
    "open_map_key": process.env.VUE_APP_OPEN_MAP_KEY,
    "open_map_street_url": process.env.VUE_APP_OSM_URL_STREET,
    "open_map_satellite_url": process.env.VUE_APP_OSM_URL_SATELLITE,
    "json_indentation": 4,
    "database": ""
  },
  "production": {
    "config_id": "production",
    "api_url": process.env.VUE_APP_API_URL,
    "socket_url": process.env.VUE_APP_SOCKET_URL,
    "app_name": "my app",
    "app_desc": "my app desc",
    "api_port": process.env.VUE_APP_API_PORT,
    "socket_port": process.env.VUE_APP_SOCKET_PORT,
    "asset_url": process.env.VUE_APP_ASSET_URL,
    "open_map_key": process.env.VUE_APP_OPEN_MAP_KEY,
    "open_map_street_url": process.env.VUE_APP_OSM_URL_STREET,
    "open_map_satellite_url": process.env.VUE_APP_OSM_URL_SATELLITE,
    "json_indentation": 4,
    "database": ""
  }
}


import axios from 'axios';
let token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhhbWlkLnVzbWFuQHlvcG1haWwuY29tIiwicG9ydGFsX2lkIjozMjYsInBvcnRhbCI6InRwYyIsImJyYW5jaCI6bnVsbCwic3RhdGlvbiI6NjUsInRpbWV6b25lIjoiQXNpYS9EdWJhaSIsImlkIjo4MjgsInBhcmVudFBvcnRhbCI6MjU0LCJvd25lcl9pZCI6ODI4LCJjb3VudHJ5IjoiQUUiLCJjdXJyZW5jeSI6IkFFRCIsImxvY2FsZSI6ImFyLUFFIiwiZmlyc3RfbmFtZSI6IkhhbWlkIiwibGFzdF9uYW1lIjoiVXNtYW4iLCJpYXQiOjE2Nzk5ODQzNTd9.MJ1qOdFB-_VGHdXTyNl1F__aqu7cv1wH4Q0v0PM5zlc';
export const api = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',

    Authorization: 'Bearer ' + token,
  },
});

export default api;



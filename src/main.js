import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import { BootstrapVue } from 'bootstrap-vue';
// TODO: remove BootstrapVue as it is not used.
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
import store from './store/index';
new Vue({
  store,
  vuetify,
  BootstrapVue,
  render: (h) => h(App),
}).$mount('#app');
